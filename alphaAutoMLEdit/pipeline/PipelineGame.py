from __future__ import print_function
import sys
import os
import pickle
import math
import logging
from copy import deepcopy
sys.path.append('..')
from ..Game import Game
from .PipelineLogic import Board
import numpy as np
from random import uniform
import traceback
import json
import time

logger = logging.getLogger(__name__)

class PipelineGame(Game):
    # FIXEME: Maybe the input parameters can be in json
    def __init__(self, pipeline=[], input={}, eval_pipeline=None):
        self.steps = 0
        self.args = input['ARGS']
        self.evaluations = {}
        self.eval_times = {}

        self.primitives = input['PRIMITIVES']
        self.pipeline_sizes = input['PIPELINE_SIZES']
        self.problem_types = input['PROBLEM_TYPES']
        self.data_types = input['DATA_TYPES']
        self.eval_pipeline = eval_pipeline

        self.problem =  input['PROBLEM'].upper()
        self.data_type = input['DATA_TYPE'].upper()
        self.metric = input['METRIC']

        self.pipeline = pipeline

        self.dataset_metafeatures = input['DATASET_METAFEATURES']
        if  self.dataset_metafeatures is None:
            metafeatures_path = args.get('metafeatures_path')
            if not metafeatures_path is None:
                metafeatures_file = os.path.join(metafeatures_path, args['dataset'] + '_metafeatures.pkl')
                if os.path.isfile(metafeatures_file):
                    m_f = open(metafeatures_file, 'rb')
                    self.dataset_metafeatures = pickle.load(m_f)[args['dataset']]

        if self.dataset_metafeatures is None:
            logger.warning('No Dataset Metafeatures specified - Initializing to empty')
            self.dataset_metafeatures = []
        else:
            self.dataset_metafeatures = list(np.nan_to_num(np.asarray(self.dataset_metafeatures)))
            
        self.m = len(self.dataset_metafeatures)+2
        self.getInitBoard()
        self.o = Board.get_edit_operations_size()
        self.action_size = 0

                
    def getInitBoard(self):
        # return initial board (numpy board)
        b = Board(self.m, self.pipeline, self.primitives, self.pipeline_sizes, self.metric)
        self.p = b.p
        self.o = b.o
        for i in range(0, len(self.dataset_metafeatures)):
            b.pieces_m[i] = self.dataset_metafeatures[i]
        b.pieces_m[i] = self.data_types[self.data_type]
        i = i+1
        b.pieces_m[i] = self.problem_types[self.problem]
        self.action_size = len(b.valid_moves)
        return b.pieces_m + b.pieces_p + b.pieces_o + b.previous_moves

    def getBoardSize(self):
        # (a,b) tuple
        return self.m + self.p + self.o

    def getActionSize(self):
        # return number of actions
        board = Board(self.m, self.pipeline, self.primitives, self.pipeline_sizes, self.metric)
        return len(board.valid_moves)

    def getNextState(self, board, player, action):
        # if player takes action on board, return next (board,player)
        # action must be a valid move
        b = Board(self.m, self.pipeline, self.primitives, self.pipeline_sizes, self.metric)
        b.pieces_m = b.get_metafeatures(board)
        b.previous_moves = b.get_previous_moves(board)
        b.execute_move(action, player)
        return (b.pieces_m+b.pieces_p+b.pieces_o+b.previous_moves, -player)

    def getValidMoves(self, board, player, ignore_prev_moves=False):
        # return a fixed size binary vector
        b = Board(self.m, self.pipeline, self.primitives, self.pipeline_sizes, self.metric)
        b.pieces_m = b.get_metafeatures(board)
        b.pieces_p = b.get_pipeline(board)
        if not ignore_prev_moves:
            b.previous_moves = b.get_previous_moves(board)
        legalMoves =  b.get_legal_moves()
        logger.info(b.pieces_p)
        logger.info([b.valid_moves[i] for i in range(0, len(legalMoves)) if legalMoves[i] == 1])
        return np.array(legalMoves)

    def getEvaluation(self, board):

        b = Board(self.m, self.pipeline, self.primitives,  self.pipeline_sizes, self.metric)
        valid_p_names = b.preprocessing_primitives_names + b.estimators_names
        valid_p_enum = b.preprocessing_primitives + b.estimators_values
        pipeline_enums = b.get_pipeline(board)
        if not any(pipeline_enums):
            return 0.0
        pipeline = [valid_p_names[valid_p_enum.index(i)] for i in pipeline_enums if
                    not i == 0]
        eval_val = self.evaluations.get(",".join(pipeline))
        if eval_val is None:
            self.steps = self.steps + 1
            try:
                eval_val = self.eval_pipeline(pipeline, 'AlphaZero eval')
            except:
                logger.warning('Error in Pipeline Execution %s', eval_val)
                traceback.print_exc()
            if eval_val is None:
                eval_val = float('inf')
            self.evaluations[",".join(pipeline)] = eval_val
            self.eval_times[",".join(pipeline)] = time.time()
        return eval_val
    
    def getGameEnded(self, board, player, eval_val=None):
        # return 0 if not ended, 1 if x won, -1 if x lost
        # player = 1

        if len(self.evaluations) > 0:
            sorted_evals = sorted([eval for eval in list(self.evaluations.values()) if eval != float('inf')])
            if len(sorted_evals) > 0:
                if 'error' in self.metric.lower():
                   win_threshold = sorted_evals[0]
                else:
                   win_threshold = sorted_evals[-1]
                b = Board(self.m, self.pipeline, self.primitives, self.pipeline_sizes, self.metric, win_threshold)
            else:
                b = Board(self.m, self.pipeline, self.primitives, self.pipeline_sizes, self.metric)
        else:
            b = Board(self.m, self.pipeline, self.primitives, self.pipeline_sizes, self.metric)
        b.pieces_m = b.get_metafeatures(board)
        b.pieces_p = b.get_pipeline(board)
        b.previous_moves = b.get_previous_moves(board)
        eval_val = self.getEvaluation(board)
        if b.findWin(player, eval_val):
            logger.info('findwin %s',player)
            return 1
        if b.findWin(-player, eval_val):
            logger.info('findwin %',-player)
            return -1
        if b.has_legal_moves():
            return 0

        return 0

    def getCanonicalForm(self, board, player):
        # return state if player==1, else return -state if player==-1
        return deepcopy(board)

    def stringRepresentation(self, board):
        # 3x3 numpy array (canonical board)
        return np.asarray(board[:self.m+self.p+self.o]).tostring()

    def getTrainExamples(self, board, pi):
        assert(len(pi) == self.getActionSize())  # 1 for pass
        eval_val = self.getEvaluation(board)
        if 'error' in self.metric.lower():
            return (board[:self.m+self.p+self.o], pi, eval_val if eval_val!= float('inf') and eval_val <= math.pow(10,15) else math.pow(10,15))
        else:
            return (board[:self.m+self.p+self.o], pi, eval_val if eval_val != float('inf') else 0)

    def display(self, b):
        n = self.p
        board = b[self.m:self.m+self.p]
        logger.info(" -----------------------")
        logger.info("PIPELINE %s", '|'.join(str(e) for e in board))
        logger.info(" -----------------------")
