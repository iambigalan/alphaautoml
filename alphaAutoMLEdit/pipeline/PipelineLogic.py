'''
Board class.
Board data:
  1=x, -1=o, 0=empty
  first dim is column , 2nd is row:
     pieces[1][2] is the square in column 2, row 3.
Squares are stored and manipulated as (x,y) tuples.
x is the column, y is the row.
'''

import numpy as np
from itertools import combinations
from scipy.special import perm
import logging

logger = logging.getLogger(__name__)

class Board():

    OPERATIONS = {0:'insert',
                  1:'delete',
                  2:'substitute'}



    def __init__(self, m=30, pipeline=None, primitives={}, pipeline_sizes={}, metric='f1macro', win_threshold=0.6):
        "Set up initial board configuration."

        self.PRIMITIVES = primitives
        self.PIPELINE_SIZES = pipeline_sizes
        
        self.m = m #Number of metafeatures
        self.p = 1 #Including the estimator primitive
        self.num_preprocessors = 0
        self.preprocessing_primitives = []
        self.preprocessing_primitives_names = []
        self.estimators_names = list(self.PRIMITIVES['ESTIMATORS'].keys())
        self.estimators_values = list(self.PRIMITIVES['ESTIMATORS'].values())
        self.valid_moves = self._compute_valid_moves(self.estimators_values, [],
                                                     self.PIPELINE_SIZES['ESTIMATORS'])

        for primitive in self.PIPELINE_SIZES.keys():
            entry = self.PRIMITIVES.get(primitive)
            if not entry is None and primitive != 'ESTIMATORS':
                primitives = entry.values()
                primitives_names = self.PRIMITIVES[primitive].keys()
                pipeline_length = self.PIPELINE_SIZES[primitive]
                self.preprocessing_primitives.extend(primitives)
                self.preprocessing_primitives_names.extend(primitives_names)
                self.p = self.p + pipeline_length
                self.num_preprocessors = self.num_preprocessors + pipeline_length
                self.valid_moves = self.valid_moves + self._compute_valid_moves(primitives,
                                                                                self.valid_moves,
                                                                                pipeline_length)
        #logger.info('NUMBER of VALID MOVES %s', len(self.valid_moves))
        
        self.previous_moves = []
        self.o = len(self.OPERATIONS)
        # Create the empty board array.
        self.pieces_m = [0] * self.m
        self.pieces_p = [0] * self.p
        if not pipeline is None:
            if len(pipeline) == 1:
               pipeline = [0, 0] + pipeline
            elif len(pipeline) == 2:
               pipeline = [0] + pipeline
        else:
            pipeline = [0] * self.p
        self.pieces_p = pipeline
        self.pieces_o = [0] * self.o
        if 'error' in metric.lower():
           win_threshold = -1 * win_threshold
        self.win_threshold = win_threshold
        self.num_estimators = len(self.PRIMITIVES['ESTIMATORS'].values())


    # add [][] indexer syntax to the Board
    def __getitem__(self, index):
        return self.pieces_p[index]

    @classmethod
    def get_edit_operations_size(cls):
        return len(cls.OPERATIONS)

    def get_pipeline(self, board):
        return board[self.m:self.m+self.p]

    def get_previous_moves(self, board):
        return board[self.m+self.p+self.o:]

    def get_metafeatures(self, board):
        return board[0:self.m]

    def get_operation(self, board):
        return board[self.m+self.p:self.m+self.p+self.o]

    def findWin(self, player, eval_val=None):
        """Find win of the given color in row, column, or diagonal
        (1 for x, -1 for o)"""
        if not any(self[0:]):
            return False
        if eval_val == float('inf'):
            return False

        return eval_val >= self.win_threshold

    def get_legal_moves(self):
        """Returns all the legal moves.
        """

        valid_moves = [0]*len(self.valid_moves)
        valid_moves = self._get_insert_moves(valid_moves)
        if any(self.pieces_p):
            valid_moves = self._get_substitue_moves(valid_moves)
        if np.where(np.asarray(self.pieces_p)>0)[0].shape[0] > 1:
            valid_moves = self._get_delete_moves(valid_moves)

        for i in range(0, len(self.previous_moves)):
            valid_moves[self.previous_moves[i]] = 0

        return valid_moves
        
    def has_legal_moves(self):
        return len(self.previous_moves) < len(self.valid_moves)

    #def _getMove(self, action):
    #   return [val for val in Board.PRIMITIVES[self.problem].values()][action]

    @classmethod
    def getPrimitives(cls):
        return self.PRIMITIVES

    @classmethod
    def getPrimitive(cls, piece):
        return self.PRIMITIVES_DECODE[piece]

    def execute_move(self, action, player):
        """Perform the given move on the board;
        color gives the color of the piece to play (1=x,-1=o)
        """
        s = self.valid_moves[action]

        s = [0] * (self.p - len(s)) + s
        pipeline = np.asarray((self.pieces_p))[np.where(np.asarray(self.pieces_p) > 0)[0]].tolist()
        if len(s) > len(pipeline):
            self.pieces_o[0] = 1
        elif len(s) < len(pipeline):
                self.pieces_o[1] = 1
        if len(s) == len(pipeline):
            self.pieces_o[2] = 1

        self.pieces_p = s
        self.previous_moves.append(action)

    def _get_insert_moves(self, valid_moves):
        if any(self.pieces_p):
            pipeline_array = np.asarray((self.pieces_p))[np.where(np.asarray(self.pieces_p) > 0)[0]]
            N = pipeline_array.shape[0]
            pipeline_set = set(pipeline_array.tolist())
        else:
            pipeline_set = set()
            N = 0

        index = 0
        for move in self.valid_moves:
            if len(move) == (N+1):
                if len(set(move).difference(pipeline_set)) == 1:
                    valid_moves[index] = 1
            index = index + 1

        return valid_moves

    def _get_delete_moves(self, valid_moves):
        if any(self.pieces_p):
            pipeline_array = np.asarray((self.pieces_p))[np.where(np.asarray(self.pieces_p) > 0)[0]]
            N = pipeline_array.shape[0]
            pipeline_set = set(pipeline_array.tolist())
        else:
            pipeline_set = set()
            N = 0

        index = 0
        for move in self.valid_moves:
            if len(move) == (N - 1):
                if len(set(move).intersection(pipeline_set)) == N-1:
                    valid_moves[index] = 1
            index = index + 1

        return valid_moves

    def _get_substitue_moves(self, valid_moves):

        if any(self.pieces_p):
            pipeline_array = np.asarray((self.pieces_p))[np.where(np.asarray(self.pieces_p) > 0)[0]]
            N = pipeline_array.shape[0]
            pipeline_set = set(pipeline_array.tolist())
        else:
            pipeline_set = set()
            N = 0

        index = 0
        for move in self.valid_moves:
            if len(move) == N:
                if len(set(move).intersection(pipeline_set)) == (N-1):
                    valid_moves[index] = 1
            index = index + 1

        return valid_moves

    def _compute_valid_moves(self, pp, est, p_len):
        valid_moves = []
        for l in range(1, p_len+1):
            c = [list(c) for c in combinations(pp, l)]
            valid_moves = valid_moves + c

        if len(est) > 0:
            tmp = valid_moves
            valid_moves = []
            for i in range(0, len(tmp)):
                for e in est:
                    valid_moves.append(tmp[i]+e)
        return est + valid_moves
